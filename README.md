# Pumpkin Field


## Description

A game created with a purpose of practicing a state machine design pattern. The goal is to gather all pumpkins without getting caught by a ghost.

States: Menu -> Game -> Win/ Lose -> Menu

## Mechanics

- After the game starts, it waits for input before the ghost starts following. 
- Arrows can be used to move around, with Shift to move faster. 
- The ghost follows the player.
- Player can gather pumpkins, scoring points.
- Points and time survived are displayed.
- Upon collision with the ghost, Lose State initiates, and displays time survived.
- Upon collision with the spiderweb, Win State initiates and displays the score.