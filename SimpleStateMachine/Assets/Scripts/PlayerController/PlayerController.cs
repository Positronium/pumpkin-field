using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float speed;
    private float superSpeed;
    
    [SerializeField] private float baseSpeed;

    public void InitialiseController()
    {
        speed = baseSpeed;
    }

    public void ResetPlayerPosition()
    {
        transform.position = Vector3.zero;
    }

    public void UpdatePlayerPosition()
    {
        float runAxisVal = Input.GetAxis("Run");

        if (runAxisVal > 0)
            speed = baseSpeed*2;
        
        else
            speed = baseSpeed;


        float xAxisVal = Input.GetAxis("Horizontal");

        transform.position += Vector3.right * xAxisVal * Time.deltaTime * speed;

        float yAxisVal = Input.GetAxis("Vertical");
        transform.position += Vector3.up * yAxisVal * Time.deltaTime * speed;
    }

  

}
