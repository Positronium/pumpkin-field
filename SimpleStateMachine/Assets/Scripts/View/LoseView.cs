using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;


public class LoseView : MonoBehaviour
{
    [SerializeField] private Button goMenuButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private TextMeshProUGUI timeText;

    public void InitializeLoseView(UnityAction LoseGameCallback, float time)
    {
        exitButton.onClick.AddListener(Application.Quit);
        goMenuButton.onClick.AddListener(LoseGameCallback);
        timeText.text = "Time: " + time.ToString();
    }
    
}
