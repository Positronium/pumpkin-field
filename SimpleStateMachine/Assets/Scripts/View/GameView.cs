using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameView : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI timeText;

    public void UpdateScore(float score)
    {
        scoreText.text = score.ToString();
    }
    public void UpdateTime(float time)
    {
        timeText.text = time.ToString();
    }

}
