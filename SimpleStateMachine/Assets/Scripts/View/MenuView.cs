using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class MenuView : MonoBehaviour
{
    [SerializeField] private Button startButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private Button creditsButton;

    public void InitializeMenuView(UnityAction startGameCallback, UnityAction creditsButtonCallback)
    {
        startButton.onClick.AddListener(startGameCallback);
        exitButton.onClick.AddListener(Application.Quit);
        creditsButton.onClick.AddListener(creditsButtonCallback);
    }
}
