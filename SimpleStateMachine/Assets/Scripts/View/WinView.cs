using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class WinView : MonoBehaviour
{
    [SerializeField] private Button goMenuButton;
    [SerializeField] private Button exitButton;
    [SerializeField] private TextMeshProUGUI winScore;
    public void InitializeWinView(UnityAction WinGameCallback, float score)
    {
        exitButton.onClick.AddListener(Application.Quit);
        goMenuButton.onClick.AddListener(WinGameCallback);

        IEnumerator ScoreCounting(float score)
        {
            float i = 0;
            float speed = 0.5f;
            while (i<=score)
            {
                winScore.text = "Score: " + i.ToString();
                i++;
                yield return new WaitForSeconds(speed);
                speed = speed - speed/3;
            }
        }

        StartCoroutine(ScoreCounting(score));
    }
}
