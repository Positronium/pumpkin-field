using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class CreditsView : MonoBehaviour
{
    [SerializeField] private Button backButton;

    public void InitializeCreditsView(UnityAction backButtonCallback)
    {
        backButton.onClick.AddListener(backButtonCallback);

    }


}
