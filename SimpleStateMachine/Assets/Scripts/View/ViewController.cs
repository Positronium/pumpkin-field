using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ViewController : MonoBehaviour
{
    [SerializeField] private MenuView menuView;
    [SerializeField] private GameView gameView;
    [SerializeField] private WinView winView;
    [SerializeField] private LoseView loseView;
    [SerializeField] private CreditsView creditsView;


    public void InitializeMenuController(UnityAction startGameCallback, UnityAction creditsButtonCallback)
    {
        menuView.InitializeMenuView(startGameCallback, creditsButtonCallback);
    }

    public void InitializeWinController(UnityAction winGameCallback, float winScore)
    {
        winView.InitializeWinView(winGameCallback, winScore);
    }

    public void InitializeLoseController(UnityAction loseGameCallback, float time)
    {
        loseView.InitializeLoseView(loseGameCallback, time);
    }

    public void InitializeGameController(float zero)
    {
        gameView.UpdateScore(zero);
        gameView.UpdateTime(zero);
    }

    public void InitializeCreditsController(UnityAction creditsCallback)
    {
        creditsView.InitializeCreditsView(creditsCallback);

    }

    public void UpdateGameController(float time)
    {
        gameView.UpdateTime(time);
    }


    // MENU VIEW HIDE AND SHOW
    public void ShowMenuView()      { menuView.gameObject.SetActive(true); }
    public void HideMenuView()      { menuView?.gameObject.SetActive(false); }
    

    // GAME VIEW HIDE AND SHOW
    public void ShowGameView()      { gameView.gameObject.SetActive(true); }
    public void HideGameView()      { gameView?.gameObject.SetActive(false); }    
    
    // WIN VIEW HIDE AND SHOW
    public void ShowWinView()      { winView.gameObject.SetActive(true); }
    public void HideWinView()      { winView?.gameObject.SetActive(false); }    
    
    // LOSE VIEW HIDE AND SHOW
    public void ShowLoseView()      { loseView.gameObject.SetActive(true); }
    public void HideLoseView()      { loseView?.gameObject.SetActive(false); }
    
    // CREDITS VIEW HIDE AND SHOW
    public void ShowCreditsView()      { creditsView.gameObject.SetActive(true); }
    public void HideCreditsView()      { creditsView?.gameObject.SetActive(false); }
}
