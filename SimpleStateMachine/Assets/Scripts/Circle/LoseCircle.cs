using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LoseCircle : MonoBehaviour
{
    [SerializeField] private AudioSource sound;

    private UnityAction onTriggerEnterListener;
    [SerializeField] private GameObject target;
    public void AddListener(UnityAction invoke) 
    {
        this.onTriggerEnterListener = invoke;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        sound.Play();
        onTriggerEnterListener.Invoke();
    }

    public void ResetLoseCirclePosition()
    {
        transform.position = new Vector3(8, -4, 0);
    }
    public void MoveLoseCircle()
    {
        transform.position = Vector3.Lerp(transform.position, target.transform.position, Time.deltaTime * 2);

    }
}
