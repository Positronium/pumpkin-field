using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class WinCircle : MonoBehaviour
{
    [SerializeField] private AudioSource sound;

    private UnityAction onTriggerEnterListener;
    public void AddListener(UnityAction callback)
    {
        this.onTriggerEnterListener = callback;
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        sound.Play();
        onTriggerEnterListener.Invoke();
    }
   
}
