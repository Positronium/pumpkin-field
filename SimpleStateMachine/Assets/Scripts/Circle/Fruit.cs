using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Fruit : MonoBehaviour
{
    private UnityAction trigger;

    private float fruitWorth = 10;
    public float GetFruitWorth()
    {
        return fruitWorth;
    }

    private ScoreSystem scoreSystem;

    private void Awake()
    {
        scoreSystem = GetComponentInParent<ScoreSystem>();
    }

    public void AddListener(UnityAction action)
    {
        trigger = action;
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        gameObject.SetActive(false);
        scoreSystem.AddPoints(fruitWorth);
        trigger.Invoke();
    }
}
