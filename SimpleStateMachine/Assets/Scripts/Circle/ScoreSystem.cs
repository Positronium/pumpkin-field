using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreSystem : MonoBehaviour
{
    [SerializeField] private AudioSource sound;

    [SerializeField] private List<Fruit> fruits = new List<Fruit>();

    private float score;

        
    private void Awake()
    {
        GetComponentsInChildren<Fruit>(fruits);
        
    }


    public void ImplementScoreSystem(UnityAction action)
    {
        foreach(Fruit fruit in fruits)
        {
            fruit.AddListener(action);
        }
    }


    private void OnEnable()
    {
        foreach (Fruit fruit in fruits)
        {
            fruit.gameObject.SetActive(true);
        }
        score = 0f;
    }

    public float GetScore()
    {
        return score;
    }

    public void AddPoints(float worth)
    {
        score += worth;
        sound.Play();
    }



    // Hide and Show View
    public void ShowTargets()
    {
        this.gameObject.SetActive(true);
    }

    public void HideTargets()
    {
        this.gameObject.SetActive(false);
    }
}
