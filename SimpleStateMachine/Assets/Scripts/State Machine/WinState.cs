using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinState : BaseState

{
    public override void InitializeState(StateMachine controller)
    {
        base.InitializeState(controller);
        controller.ViewController.ShowWinView();
        controller.ViewController.InitializeWinController(()=>controller.ChangeState( new MenuState()), controller.ScoreSystem.GetScore());
    }
    public override void UpdateState()
    {

    }
    public override void DestroyState()
    {
        controller.ViewController.HideWinView();
    }

}
