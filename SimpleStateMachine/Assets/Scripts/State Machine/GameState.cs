using UnityEngine;
using System.Collections;
using System;

public class GameState : BaseState
{
    private bool waitForButton = true;
    public override void InitializeState(StateMachine controller)

    {
        base.InitializeState(controller);

        controller.ViewController.ShowGameView();
        controller.ViewController.InitializeGameController(0);

        controller.PlayerController.InitialiseController();
        controller.PlayerController.ResetPlayerPosition();

        controller.LoseCircle.ResetLoseCirclePosition();

        controller.WinCircle.AddListener(() => controller.ChangeState(new WinState()));
        controller.LoseCircle.AddListener(() => controller.ChangeState(new LoseState()));

        controller.ScoreSystem.ImplementScoreSystem(() => controller.ViewController.InitializeGameController(controller.ScoreSystem.GetScore()));
        controller.TimeSystem.InitializeTimeSystem();

        controller.ScoreSystem.ShowTargets();
    }

    public override void UpdateState()
    {
        if (!waitForButton)
        {

            controller.PlayerController.UpdatePlayerPosition();

            controller.ViewController.UpdateGameController(controller.TimeSystem.GetTime());
            controller.LoseCircle.MoveLoseCircle();
        }
        else
        {
            if (Input.anyKeyDown)
                waitForButton = false;
        }

    }

    public override void DestroyState()
    {
        controller.TimeSystem.StopTimeSystem();
        controller.ViewController.HideGameView();
        controller.ScoreSystem.HideTargets();

    }
    

}
