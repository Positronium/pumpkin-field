using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoseState : BaseState
{
    public override void InitializeState(StateMachine controller)
    {
        base.InitializeState(controller);
        controller.ViewController.ShowLoseView();
        controller.ViewController.InitializeLoseController(() => controller.ChangeState(new MenuState()), controller.TimeSystem.GetTime());
    }

    public override void UpdateState()
    {

    }
    public override void DestroyState()
    {
        controller.ViewController.HideLoseView();
    }

}
