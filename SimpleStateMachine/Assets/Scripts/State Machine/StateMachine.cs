using UnityEngine;

public class StateMachine : MonoBehaviour
{

    // View Controller
    [SerializeField] private ViewController viewController;
    public ViewController ViewController => viewController;

    //Player Controller
    [SerializeField] private PlayerController playerController;
    public PlayerController PlayerController => playerController;

    [SerializeField] private WinCircle winCircle;
    public WinCircle WinCircle => winCircle;

    [SerializeField] private LoseCircle loseCircle;
    public LoseCircle LoseCircle => loseCircle;

    [SerializeField] private ScoreSystem scoreSystem;
    public ScoreSystem ScoreSystem => scoreSystem;

    [SerializeField] private TimeSystem timeSystem;
    public TimeSystem TimeSystem => timeSystem;




    // State Management

    private BaseState currentlyActiveState;
    public void ChangeState(BaseState newState)
    {
        currentlyActiveState?.DestroyState();
        
        currentlyActiveState = newState;
        currentlyActiveState?.InitializeState(this);
    }
    private void Start()
    {
        ChangeState(new MenuState());
    }

    private void Update()
    {
        currentlyActiveState?.UpdateState();

    }

    private void OnDestroy()
    {
        currentlyActiveState?.DestroyState();
    }
}
