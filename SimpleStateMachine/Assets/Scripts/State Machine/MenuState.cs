using UnityEngine;
public class MenuState : BaseState
{

    public override void InitializeState(StateMachine controller)
    {
        base.InitializeState(controller);
        controller.ViewController.ShowMenuView();
        controller.ViewController.InitializeMenuController(() => controller.ChangeState(new GameState()), ()=> controller.ViewController.ShowCreditsView());
        controller.ViewController.InitializeCreditsController(()=> controller.ViewController.HideCreditsView());


    }

    public override void UpdateState()
    {

    }

    public override void DestroyState()
    {
        controller.ViewController.HideMenuView();
    }

}
