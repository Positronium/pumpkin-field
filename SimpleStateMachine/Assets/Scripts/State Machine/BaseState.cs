public abstract class BaseState
{
    protected StateMachine controller;


    public virtual void InitializeState(StateMachine controller)
    {
        this.controller = controller;
    }

    public abstract void UpdateState();

    public abstract void DestroyState();

}
