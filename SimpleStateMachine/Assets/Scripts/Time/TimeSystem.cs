using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class TimeSystem : MonoBehaviour
{
    private float time;
    public float GetTime()
    {
        return time;
    }
    
    public void InitializeTimeSystem()
    {
        time = 0;
        StartCoroutine(CountingTime());
    }
    public void StopTimeSystem()
    {
        StopAllCoroutines();
    }

    

    public IEnumerator CountingTime()
    {

        while (true)
        {
            yield return new WaitForSecondsRealtime(1f);
            time++;
        }
    }
}
